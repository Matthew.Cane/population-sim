import random, csv, time

maleNames = []
femaleNames = []
surnames = []
deathRateMale = {}
deathRateFemale = {}
births = {}


def getName(gender):
    if gender == 1:
        return maleNames[random.randint(0,len(maleNames)-1)]
    else:
        return femaleNames[random.randint(0,len(femaleNames)-1)]

def getSurname():
    return surnames[random.randint(0,len(surnames)-1)]

def loadNames():
    with open('male.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            maleNames.append(row[2])

    with open('female.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            femaleNames.append(row[2])

    with open('surnames.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            surnames.append(row[1])

def loadData():
    with open('deathMale.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            deathRateMale[row[0]] = row[1]

    with open('deathFemale.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            deathRateFemale[row[0]] = row[1]

    with open('births.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            births[row[0]] = row[1]

def hasDied(person):
    if person.gender == 1:
        if person.age > 84:
            if random.randint(0,6) == 0:
                return True
            else:
                return False
        if random.randint(0,int(deathRateMale[str(person.age)])) == 0:
            return True
    elif person.age > 84:
        if random.randint(0,6) == 0:
            return True
        else:
            return False
        if random.randint(0,int(deathRateFemale[str(person.age)])) == 0:
            return True
    return False

def hasBirthed(person):
    if person.age < 18 or person.age > 45:
        return False

    if random.randint(0, 1000) > float(births[str(person.age)]):
        return True
    return False

def choosePartner(mother, population):
    ageMin = (mother.age/2)+7
    ageMax = (mother.age-7)*2
    popTemp = population
    random.shuffle(popTemp)
    for person in popTemp:
        if person.gender == 0:
            continue
        if person.age < ageMin:
            continue
        if person.age > ageMax:
            continue
        if person.surname == mother.surname:
            continue
        return person
    return False

def logYearSummary(logID, population, year):
    logName = "logs/"+logID+"-log.csv"
    log = open(logName, "a")
    log.write(str(year)+","+str(len(population))+"\n")
    log.close()

def printPop(population, output):
    for person in population:
        person.print(output)

def printMaternalDecendents(person, output):
    output.insert("end", person.printName()+"'S MATERNAL DECENDENTS:")
    if person.mother == None:
        output.insert(END, "\n"+person.printName()+" HAS NO MATERNAL DECEDENT")
        return
    mother = person.mother
    while mother != None:
        output.insert("end", "\n"+person.mother.printName()+" IS THE MOTHER OF "+person.printName())
        person = person.mother
        mother = person.mother

def printPaternalDecendents(person, output):
    output.insert("end", "\n\n"+person.printName()+"'S PATERNAL DECENDENTS:")
    if person.father == None:
        output.insert(END, "\n"+person.printName()+" HAS NO PATERNAL DECEDENT")
        return
    father = person.father
    while father != None:
        output.insert("end", "\n"+person.father.printName()+" IS THE FATHER OF "+person.printName())
        person = person.father
        father = person.father

def getPopSize(population):
    return str(len(population))

def getPerson(name, population):
    for person in population:
        if person.getFullName() == name:
            return person
    return None

def getFamily(name, population):
    people = []
    for person in population:
        if person.surname == name:
            people.append(person)
    if not people:
        return None
    return people
