import random
from functions import *

class Person:
    age = 0
    name = ""
    surname = ""
    gender = 0
    mother = None
    father = None

    def __init__(self):
        self.age = random.randint(10,80)
        self.gender = random.randint(0,1)
        self.name = getName(self.gender)
        self.surname = getSurname()

    def print(self, output):
        output.insert("end", "\n"+self.name+" "+self.surname+", "+self.genderString()+", "+str(self.age))

    def printName(self):
        return self.name+" "+self.surname

    def genderString(self):
        if self.gender == 1:
            return "Male"
        else:
            return "Female"

    def getFullName(self):
        return self.name+" "+self.surname
