# Population Simulator

This was written in my spare time over a week or so the summer of 2020 at an attempt to learn how to implement a basic Python UI and to practice writing OO Python.

It takes into account average birth and death rates for a given age range from real world data, and calculates deaths and births for each individual over a given number of years. You are also able to look up the descendants of any person and the members of a particular family.

## How To Run

1. Ensure you have Python 3 installed.
2. Download and extract the source code.
3. Run the file sim.py.

## How To Use

1. Enter a starting population size and the number of years you want the simulation to run for.
2. Click 'Start Simulation' to run. The window will then display the events of each year as well as a final summary of the population.
3. Enter the full name of a person into the 'Show Decendents Of' box to show all their decendents.
4. Enter a family name into the 'Show Members Of Family' box to show all members of that family.
5. To return to the last run simulation, click 'Display Previous Simulation'.
6. A file with the size of the population in a given year can be found in the logs directory.
