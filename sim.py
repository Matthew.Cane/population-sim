import random, time
from tkinter import *
from person import Person
from functions import *

population = []
simLog = "\nNO PREVIOUS SIMULATION TO DISPLAY"

def startSimulation(startSize, simLength):
    global population
    population = []
    global simLog
    logID = str(int(time.time()))
    startTime = time.monotonic_ns()
    year = time.gmtime()[0]
    random.seed(time.monotonic_ns())
    for i in range(startSize):
        population.append(Person())

    startingPop = population
    output.insert(END, "STARTING POPULATION:\n\n")

    printPop(population, output)

    for i in range(simLength):
        output.insert(END, "\n\nYEAR: "+str(year))
        output.insert(END, "\n---------------------------\n")
        for person in population:
            if hasDied(person):
                output.insert(END, "\n"+person.printName()+" HAS DIED AGED "+str(person.age))
                population.remove(person)
                continue
            if person.gender == 0:
                if hasBirthed(person):
                    father = choosePartner(person, population)
                    if father == False:
                        continue
                    child = Person()
                    child.surname = father.surname
                    child.age = 0
                    child.mother = person
                    child.father = father
                    population.append(child)
                    output.insert(END, "\n"+person.printName()+" AND "+ father.printName()+" HAVE HAD A CHILD "+child.printName())
            person.age = person.age+1

        logYearSummary(logID, population, year)
        year = year + 1
    finalPop = population
    output.insert(END, "\n\nFINAL POPULATION:\n\n")
    printPop(finalPop, output)
    runTime = (time.monotonic_ns() - startTime) / 1000000
    output.insert(END, "\n\n"+"SIMULATION TOOK: "+str(runTime)+"ms")
    simLog = output.get("1.0", END)



loadNames()
loadData()

def startButton():
    output.delete("1.0", END)
    size = int(startSize.get())
    length = int(simLength.get())
    startSimulation(size, length)

def decendentsButton():
    output.delete("1.0", END)
    name = str(decendents.get())
    person = getPerson(name, population)
    if person:
        printMaternalDecendents(person, output)
        printPaternalDecendents(person, output)
        return
    output.insert(END, "\nNO PERSON WITH NAME "+name+" FOUND IN POPULATION")

def replayButton():
    output.delete("1.0", END)
    output.insert(END, simLog)

def familyButton():
    output.delete("1.0", END)
    surname = family.get()
    surname = surname.upper()
    print(surname)
    familyMembers = getFamily(surname, population)
    if familyMembers is None:
        output.insert(END, "\nNO PEOPLE WITH SURNAME "+surname+" FOUND IN POPULATION")
        return
    for person in familyMembers:
        person.print(output)

window = Tk()
window.title("Population Simulation")

#Start pop
Label(window, text="Starting Population:", font="none 12 bold").grid(row=0, column=0, sticky=W)
startSize = Entry(window)
startSize.grid(row=0, column=1, sticky=W)

#Sim length
Label(window, text="Simulation Length:", font="none 12 bold").grid(row=1, column=0, sticky=W)
simLength = Entry(window)
simLength.grid(row=1, column=1, sticky=W)

Button(window, text="Start Simulation", command=startButton).grid(row=2, column=1, sticky=W)

#Decendents
Label(window, text="Show Decendents Of:", font="none 12 bold").grid(row=0, column=2, sticky=W)
decendents = Entry(window)
decendents.grid(row=0, column=3, sticky=W)
Button(window, text="Find Decendents", command=decendentsButton).grid(row=0, column=4, sticky=W)

#Family
Label(window, text="Show Members Of Family:", font="none 12 bold").grid(row=1, column=2, sticky=W)
family = Entry(window)
family.grid(row=1, column=3, sticky=W)
Button(window, text="Find Family Members", command=familyButton).grid(row=1, column=4, sticky=W)

Button(window, text="Display Previous Simulation", command=replayButton).grid(row=2, column=2, sticky=W)

output = Text(window, wrap=WORD)
output.grid(row=3, column=0, sticky=W, columnspan=4)

startSize.insert(END, "10")
simLength.insert(END, "20")
window.mainloop()
